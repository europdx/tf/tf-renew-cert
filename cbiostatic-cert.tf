provider "acme" {
#  server_url = "https://acme-staging-v02.api.letsencrypt.org/directory"
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}

resource "tls_private_key" "private_key" {
  algorithm = "RSA"
}

resource "acme_registration" "reg" {
  account_key_pem = tls_private_key.private_key.private_key_pem
  email_address   = "pesa@ics.muni.cz"
}

resource "acme_certificate" "certificate" {
  account_key_pem = acme_registration.reg.account_key_pem
  common_name     = "letsencrypt.edirex.ics.muni.cz"


  dns_challenge {
    provider = "rfc2136"
    config = {
      RFC2136_NAMESERVER     = "ns.muni.cz"
      RFC2136_TSIG_ALGORITHM = "hmac-md5.sig-alg.reg.int."
      RFC2136_TSIG_KEY       = "edirex.ics.muni.cz."
      RFC2136_TSIG_SECRET    = var.dns_key_secret
    }
}
}

resource "local_file" "key" {
    content     = acme_certificate.certificate.private_key_pem
    filename = "privkey.pem"
}

resource "local_file" "fullchain" {
    content     = "${acme_certificate.certificate.certificate_pem}${acme_certificate.certificate.issuer_pem}"
    filename = "fullchain.pem"
}


output "private_key" {
  value = acme_certificate.certificate.private_key_pem
}

output "certificate" {
  value = acme_certificate.certificate.certificate_pem
}

output "issuer" {
  value = acme_certificate.certificate.issuer_pem
}

