# Usage


```
terraform apply
kubectl create secret tls letsencrypt-certs -n cbio-on-demand-sandbox --key privkey.pem --cert fullchain.pem
```